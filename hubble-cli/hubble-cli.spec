%global _missing_build_ids_terminate_build 0
%global debug_package %{nil}
%define binname hubble

Name:           hubble-cli
Version:        1.17.1
Release:        1%{?dist}
Summary:        Network, Service & Security Observability for Kubernetes using eBPF

License:        Apache-2.0
URL:            https://github.com/cilium/%{binname}
Source0:        %{url}/archive/refs/tags/v%{version}.tar.gz

BuildRequires:  git
BuildRequires:  golang >= 1.21
BuildRequires:  go-md2man

Provides:       %{name} = %{version}

%description
Hubble is a fully distributed networking and security observability platform for cloud native workloads. It is built on top of Cilium and eBPF to enable deep visibility into the communication and behavior of services as well as the networking infrastructure in a completely transparent manner.


%prep
%autosetup -n %{binname}-%{version}


%build
%make_build %{binname}

mkdir completion
./%{binname} completion bash > completion/%{binname}
./%{binname} completion zsh > completion/_%{binname}
./%{binname} completion fish > completion/%{binname}.fish


%install
rm -rf $RPM_BUILD_ROOT
%make_install BINDIR=%{_bindir}
install -Dpm 644 completion/%{binname} -t %{buildroot}%{_datadir}/bash-completion/completions/
install -Dpm 644 completion/_%{binname} -t %{buildroot}%{_datadir}/zsh/site-functions/
install -Dpm 644 completion/%{binname}.fish -t %{buildroot}%{_datadir}/fish/vendor_completions.d/


%files
%license LICENSE
%doc *.md
%{_bindir}/%{binname}
%{_datadir}


%changelog
* Wed Feb 12 2025 Matyáš Kroupa <kroupa.matyas@gmail.com> - 1.17.1-1
- Update to 1.17.1

* Sat Feb 08 2025 Matyáš Kroupa <kroupa.matyas@gmail.com> - 1.17.0-1
- Update to 1.17.0

* Thu Jan 23 2025 Matyáš Kroupa <kroupa.matyas@gmail.com> - 1.16.6-1
- Update to 1.16.6

* Thu Dec 19 2024 Matyáš Kroupa <kroupa.matyas@gmail.com> - 1.16.5-1
- Update to 1.16.5

* Thu Nov 21 2024 Matyáš Kroupa <kroupa.matyas@gmail.com> - 1.16.4-1
- Update to 1.16.4

* Mon Oct 28 2024 Matyáš Kroupa <kroupa.matyas@gmail.com> - 1.16.3-1
- Update to 1.16.3

* Fri Oct 04 2024 Matyáš Kroupa <kroupa.matyas@gmail.com> - 1.16.2-1
- Update to 1.16.2

* Fri Sep 13 2024 Matyáš Kroupa <kroupa.matyas@gmail.com> - 1.16.1-1
- Update to 1.16.1

* Thu Jul 25 2024 Matyáš Kroupa <kroupa.matyas@gmail.com> - 1.16.0-1
- Update to 1.16.0

* Thu Jul 18 2024 Matyáš Kroupa <kroupa.matyas@gmail.com> - 0.13.6-1
- Update to 0.13.6

* Thu Jun 06 2024 Matyáš Kroupa <kroupa.matyas@gmail.com> - 0.13.5-1
- Update to 0.13.5

* Mon May 13 2024 Matyáš Kroupa <kroupa.matyas@gmail.com> - 0.13.4-1
- Update to 0.13.4

* Thu Apr 18 2024 Matyáš Kroupa <kroupa.matyas@gmail.com> - 0.13.3-1
- Update to 0.13.3

* Mon Mar 18 2024 Matyáš Kroupa <kroupa.matyas@gmail.com> - 0.13.2-2
- Add shell completions

* Mon Mar 11 2024 Matyáš Kroupa <kroupa.matyas@gmail.com> - 0.13.2-1
- Update to 0.13.2

* Mon Mar 11 2024 Matyáš Kroupa <kroupa.matyas@gmail.com> - 0.13.1-1
- Update to 0.13.1

* Sun Feb 11 2024 Matyáš Kroupa <kroupa.matyas@gmail.com>
- Initial version
