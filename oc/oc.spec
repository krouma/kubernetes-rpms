%global releasedate 202502040032
%global debug_package %{nil}

Name:           oc
Version:        4.18.0
Release:        1%{?dist}
Summary:        The OpenShift Command Line, part of OKD

License:        Apache-2.0
URL:            https://github.com/openshift/%{name}
Source0:        %{url}/archive/refs/tags/openshift-clients-%{version}-%{releasedate}.tar.gz

BuildRequires:  golang >= 1.21
BuildRequires:  gpgme-devel
BuildRequires:  libassuan-devel
BuildRequires:  krb5-devel

Provides:       %{name} = %{version}

%description
With OpenShift Client CLI (oc), you can create applications and manage OpenShift resources. It is built on top of kubectl which means it provides its full capabilities to connect with any kubernetes compliant cluster, and on top adds commands simplifying interaction with an OpenShift cluster.


%prep
%setup -n oc-openshift-clients-%{version}-%{releasedate}


%build
%make_build %{name} SOURCE_GIT_TAG=v%{version}

mkdir completion
./%{name} completion bash > completion/%{name}
./%{name} completion zsh > completion/_%{name}
./%{name} completion fish > completion/%{name}.fish


%install
install -m 755 -vd %{buildroot}/%{_bindir}
install -m 755 -vp %{name} %{buildroot}/%{_bindir}
install -Dpm 644 completion/%{name} -t %{buildroot}%{_datadir}/bash-completion/completions/
install -Dpm 644 completion/_%{name} -t %{buildroot}%{_datadir}/zsh/site-functions/
install -Dpm 644 completion/%{name}.fish -t %{buildroot}%{_datadir}/fish/vendor_completions.d/


%files
%license LICENSE
%doc *.md
%{_bindir}/%{name}
%{_datadir}


%changelog
* Sat Mar 01 2025 Matyáš Kroupa <kroupa.matyas@gmail.com> - 4.18.0-1
- Update to 4.18.0

* Thu Feb 20 2025 Matyáš Kroupa <kroupa.matyas@gmail.com> - 4.17.0-1
- Update to 4.17.0

* Mon Sep 23 2024 Matyáš Kroupa <kroupa.matyas@gmail.com> - 4.16.0-1
- Update to 4.16.0

* Mon Mar 18 2024 Matyáš Kroupa <kroupa.matyas@gmail.com> - 4.15.0-2
- Add shell completions

* Thu Feb 29 2024 Matyáš Kroupa <kroupa.matyas@gmail.com> - 4.15.0-1
- Update to 4.15.0

* Sun Feb 25 2024 Matyáš Kroupa <kroupa.matyas@gmail.com> - 4.14.0-2
- Download tarball instead of cloning repo

* Sun Feb 18 2024 Matyáš Kroupa <kroupa.matyas@gmail.com>
- Initial version
