%global _missing_build_ids_terminate_build 0
%global debug_package %{nil}
%define binname flux

Name:           flux2-cli
Version:        2.5.1
Release:        1%{?dist}
Summary:        Open and extensible continuous delivery solution for Kubernetes. Powered by GitOps Toolkit.

License:        Apache-2.0
URL:            https://github.com/fluxcd/flux2
Source0:        %{url}/archive/refs/tags/v%{version}.tar.gz

BuildRequires:  git
BuildRequires:  golang >= 1.20
BuildRequires:  go-md2man
BuildRequires:  kustomize
BuildRequires:  make

Provides:       %{name} = %{version}
Provides:       %{binname} = %{version}

%description
Flux is a tool for keeping Kubernetes clusters in sync with sources of configuration (like Git repositories and OCI artifacts), and automating updates to configuration when there is new code to deploy.

Flux version 2 ("v2") is built from the ground up to use Kubernetes' API extension system, and to integrate with Prometheus and other core components of the Kubernetes ecosystem. In version 2, Flux supports multi-tenancy and support for syncing an arbitrary number of Git repositories, among other long-requested features.

Flux v2 is constructed with the GitOps Toolkit, a set of composable APIs and specialized tools for building Continuous Delivery on top of Kubernetes.


%prep
%autosetup -n flux2-%{version}


%build
export VERSION="%{version}"
%make_build build

mkdir completion
./bin/%{binname} completion bash > completion/%{binname}
./bin/%{binname} completion zsh > completion/_%{binname}
./bin/%{binname} completion fish > completion/%{binname}.fish


%install
install -D -p -m 0755 bin/%{binname} %{buildroot}%{_bindir}/%{binname}
install -Dpm 644 completion/%{binname} -t %{buildroot}%{_datadir}/bash-completion/completions/
install -Dpm 644 completion/_%{binname} -t %{buildroot}%{_datadir}/zsh/site-functions/
install -Dpm 644 completion/%{binname}.fish -t %{buildroot}%{_datadir}/fish/vendor_completions.d/


%files
%license LICENSE
%doc *.md
%{_bindir}/%{binname}
%{_datadir}


%changelog
* Wed Feb 26 2025 Matyáš Kroupa <kroupa.matyas@gmail.com> - 2.5.1-1
- Update to 2.5.1

* Thu Feb 20 2025 Matyáš Kroupa <kroupa.matyas@gmail.com> - 2.5.0-1
- Update to 2.5.0

* Tue Oct 01 2024 Matyáš Kroupa <kroupa.matyas@gmail.com> - 2.4.0-1
- Update to 2.4.0

* Wed May 01 2024 Matyáš Kroupa <kroupa.matyas@gmail.com>
- Initial version
