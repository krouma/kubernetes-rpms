%global _missing_build_ids_terminate_build 0
%global debug_package %{nil}
%define binname cilium

Name:           cilium-cli
Version:        0.18.1
Release:        1%{?dist}
Summary:        CLI to install, manage & troubleshoot Kubernetes clusters running Cilium

License:        Apache-2.0
URL:            https://github.com/cilium/%{name}
Source0:        %{url}/archive/refs/tags/v%{version}.tar.gz

BuildRequires:  git
BuildRequires:  golang >= 1.22
BuildRequires:  go-md2man

Provides:       %{name} = %{version}

%description
Cilium is open source software for providing and transparently securing network connectivity and loadbalancing between application workloads such as application containers or processes. Cilium operates at Layer 3/4 to provide traditional networking and security services as well as Layer 7 to protect and secure use of modern application protocols such as HTTP, gRPC and Kafka. Cilium is integrated into common orchestration frameworks such as Kubernetes.


%prep
%autosetup


%build
%make_build %{binname}

mkdir completion
./%{binname} completion bash > completion/%{binname}
./%{binname} completion zsh > completion/_%{binname}
./%{binname} completion fish > completion/%{binname}.fish


%install
rm -rf $RPM_BUILD_ROOT
%make_install BINDIR=%{_bindir}
install -Dpm 644 completion/%{binname} -t %{buildroot}%{_datadir}/bash-completion/completions/
install -Dpm 644 completion/_%{binname} -t %{buildroot}%{_datadir}/zsh/site-functions/
install -Dpm 644 completion/%{binname}.fish -t %{buildroot}%{_datadir}/fish/vendor_completions.d/


%files
%license LICENSE
%doc *.md
%{_bindir}/%{binname}
%{_datadir}


%changelog
* Fri Mar 07 2025 Matyáš Kroupa <kroupa.matyas@gmail.com> - 0.18.1-1
- Update to 0.18.1

* Sun Mar 02 2025 Matyáš Kroupa <kroupa.matyas@gmail.com> - 0.18.0-1
- Update to 0.18.0

* Sat Feb 22 2025 Matyáš Kroupa <kroupa.matyas@gmail.com> - 0.17.0-1
- Update to 0.17.0

* Thu Jan 30 2025 Matyáš Kroupa <kroupa.matyas@gmail.com> - 0.16.24-1
- Update to 0.16.24

* Thu Jan 09 2025 Matyáš Kroupa <kroupa.matyas@gmail.com> - 0.16.23-1
- Update to 0.16.23

* Wed Dec 11 2024 Matyáš Kroupa <kroupa.matyas@gmail.com> - 0.16.22-1
- Update to 0.16.22

* Fri Dec 06 2024 Matyáš Kroupa <kroupa.matyas@gmail.com> - 0.16.21-1
- Update to 0.16.21

* Sat Nov 09 2024 Matyáš Kroupa <kroupa.matyas@gmail.com> - 0.16.20-1
- Update to 0.16.20

* Wed Oct 02 2024 Matyáš Kroupa <kroupa.matyas@gmail.com> - 0.16.19-1
- Update to 0.16.19

* Fri Sep 20 2024 Matyáš Kroupa <kroupa.matyas@gmail.com> - 0.16.18-1
- Update to 0.16.18

* Sat Sep 14 2024 Matyáš Kroupa <kroupa.matyas@gmail.com> - 0.16.17-1
- Update to 0.16.17

* Tue Aug 20 2024 Matyáš Kroupa <kroupa.matyas@gmail.com> - 0.16.16-1
- Update to 0.16.16

* Wed Aug 07 2024 Matyáš Kroupa <kroupa.matyas@gmail.com> - 0.16.15-1
- Update to 0.16.15

* Wed Jul 31 2024 Matyáš Kroupa <kroupa.matyas@gmail.com> - 0.16.14-1
- Update to 0.16.14

* Sun Jul 14 2024 Matyáš Kroupa <kroupa.matyas@gmail.com> - 0.16.13-1
- Update to 0.16.13

* Thu Jun 27 2024 Matyáš Kroupa <kroupa.matyas@gmail.com> - 0.16.11-1
- Update to 0.16.11

* Sat Jun 08 2024 Matyáš Kroupa <kroupa.matyas@gmail.com> - 0.16.10-1
- Update to 0.16.10

* Fri May 31 2024 Matyáš Kroupa <kroupa.matyas@gmail.com> - 0.16.9-1
- Update to 0.16.9

* Thu May 30 2024 Matyáš Kroupa <kroupa.matyas@gmail.com> - 0.16.8-1
- Update to 0.16.8

* Tue May 07 2024 Matyáš Kroupa <kroupa.matyas@gmail.com> - 0.16.7-1
- Update to 0.16.7

* Tue Apr 30 2024 Matyáš Kroupa <kroupa.matyas@gmail.com> - 0.16.6-1
- Update to 0.16.6

* Fri Apr 26 2024 Matyáš Kroupa <kroupa.matyas@gmail.com> - 0.16.5-1
- Update to 0.16.5

* Thu Mar 28 2024 Matyáš Kroupa <kroupa.matyas@gmail.com> - 0.16.4-1
- Update to 0.16.4

* Mon Mar 18 2024 Matyáš Kroupa <kroupa.matyas@gmail.com> - 0.16.3-1
- Update to 0.16.3

* Mon Mar 18 2024 Matyáš Kroupa <kroupa.matyas@gmail.com> - 0.16.2-2
- Add shell completions

* Sat Mar 16 2024 Matyáš Kroupa <kroupa.matyas@gmail.com> - 0.16.2-1
- Update to 0.16.2

* Thu Mar 14 2024 Matyáš Kroupa <kroupa.matyas@gmail.com> - 0.16.1-1
- Update to 0.16.1

* Sat Mar 09 2024 Matyáš Kroupa <kroupa.matyas@gmail.com> - 0.16.0-1
- Update to 0.16.0

* Sun Feb 18 2024 Matyáš Kroupa <kroupa.matyas@gmail.com> - 0.15.23-2
- Update required golang version

* Sun Feb 18 2024 Matyáš Kroupa <kroupa.matyas@gmail.com> - 0.15.23-1
- Update to 0.15.23

* Fri Feb 02 2024 Matyáš Kroupa <kroupa.matyas@gmail.com> - 0.15.22-1
- Update to 0.15.22

* Wed Jan 31 2024 Matyáš Kroupa <kroupa.matyas@gmail.com> - 0.15.21-1
- Update to 0.15.21

* Thu Jan 11 2024 Matyáš Kroupa <kroupa.matyas@gmail.com> - 0.15.20-1
- Update to 0.15.20

* Mon Dec 18 2023 Matyáš Kroupa <kroupa.matyas@gmail.com> - 0.15.19-1
- Update to 0.15.19

* Wed Dec 13 2023 Matyáš Kroupa <kroupa.matyas@gmail.com> - 0.15.18-1
- Update to 0.15.18

* Tue Dec 05 2023 Matyáš Kroupa <kroupa.matyas@gmail.com> - 0.15.17-1
- Update to 0.15.17

* Wed Nov 29 2023 Matyáš Kroupa <kroupa.matyas@gmail.com> - 0.15.16-1
- Update to 0.15.16

* Tue Nov 28 2023 Matyáš Kroupa <kroupa.matyas@gmail.com> - 0.15.15-1
- Update to 0.15.15

* Thu Nov 16 2023 Matyáš Kroupa <kroupa.matyas@gmail.com> - 0.15.14-1
- Update to 0.15.14

* Wed Nov 08 2023 Matyáš Kroupa <kroupa.matyas@gmail.com> - 0.15.13-1
- Update to 0.15.13

* Thu Nov 02 2023 Matyáš Kroupa <kroupa.matyas@gmail.com> - 0.15.12-1
- Update to 0.15.12

* Tue Oct 17 2023 Matyáš Kroupa <kroupa.matyas@gmail.com> - 0.15.11-1
- Update to 0.15.11

* Sun Oct 08 2023 Matyáš Kroupa <kroupa.matyas@gmail.com> - 0.15.10-1
- Update to 0.15.10

* Thu Oct 05 2023 Matyáš Kroupa <kroupa.matyas@gmail.com> - 0.15.9-1
- Update to 0.15.9

* Wed Sep 13 2023 Matyáš Kroupa <kroupa.matyas@gmail.com> - 0.15.8-1
- Update to 0.15.8

* Sun Sep 03 2023 Matyáš Kroupa <kroupa.matyas@gmail.com> - 0.15.7-1
- Update to 0.15.7

* Wed Aug 23 2023 Matyáš Kroupa <kroupa.matyas@gmail.com> - 0.15.6-1
- Update to 0.15.6

* Wed Aug 02 2023 Matyáš Kroupa <kroupa.matyas@gmail.com> - 0.15.5-1
- Update to 0.15.5

* Tue Jul 25 2023 Matyáš Kroupa <kroupa.matyas@gmail.com> - 0.15.4-1
- Update to 0.15.4

* Wed Jun 28 2023 Matyáš Kroupa <kroupa.matyas@gmail.com> - 0.15.0-1
- Update to 0.15.0

* Fri Jun 23 2023 Matyáš Kroupa <kroupa.matyas@gmail.com> - 0.14.8-1
- Update to 0.14.8

* Wed Jun 21 2023 Matyáš Kroupa <kroupa.matyas@gmail.com> - 0.14.7-2
- Require git during build

* Wed Jun 21 2023 Matyáš Kroupa <kroupa.matyas@gmail.com> - 0.14.7-1
- Update to 0.14.7

* Sat Feb 25 2023 Matyáš Kroupa <kroupa.matyas@gmail.com> - 0.13.0-1
- Update to 0.13.0

* Wed Jan 25 2023 Matyáš Kroupa <kroupa.matyas@gmail.com> - 0.12.12-1
- Update to 0.12.12

* Fri Dec 02 2022 Matyáš Kroupa <kroupa.matyas@gmail.com> - 0.12.11-1
- Update to 0.12.11

* Fri Nov 11 2022 Matyáš Kroupa <kroupa.matyas@gmail.com> - 0.12.6-1
- Update to 0.12.6

* Wed Oct 19 2022 Matyáš Kroupa <kroupa.matyas@gmail.com> - 0.12.5-1
- Update to 0.12.5

* Thu Sep 22 2022 Matyáš Kroupa <kroupa.matyas@gmail.com> - 0.12.4-1
- Update to 0.12.4

* Fri Aug 19 2022 Matyáš Kroupa <kroupa.matyas@gmail.com> - 0.12.1-1
- Update to 0.12.1

* Wed Jul 20 2022 Matyáš Kroupa <kroupa.matyas@gmail.com> - 0.12.0-1
- Update to 0.12.0

* Mon Jul 18 2022 Matyáš Kroupa <kroupa.matyas@gmail.com> - 0.11.11-1
- Update to 0.11.11

* Tue May 17 2022 Matyáš Kroupa <kroupa.matyas@gmail.com> - 0.11.7-1
- Update to 0.11.7

* Sat May 14 2022 Matyáš Kroupa <kroupa.matyas@gmail.com> - 0.11.6-1
- Fix major version

* Sat May 14 2022 Matyáš Kroupa <kroupa.matyas@gmail.com> - 1.11.6-1
- Update to 1.11.6

* Fri Apr 15 2022 Matyáš Kroupa <kroupa.matyas@gmail.com> - 0.11.1-1
- Update to 0.11.1

* Wed Dec 15 2021 Matyáš Kroupa <kroupa.matyas@gmail.com>
- Initial version
