%global _missing_build_ids_terminate_build 0
%global debug_package %{nil}
%define cli_binname tetra

Name:           tetragon
Version:        1.3.0
Release:        1%{?dist}
Summary:        eBPF-based Security Observability and Runtime Enforcement

License:        Apache-2.0
URL:            https://github.com/cilium/%{name}
Source0:        %{url}/archive/refs/tags/v%{version}.tar.gz
Source101:      %{name}.sysusers

BuildRequires:  git
BuildRequires:  golang >= 1.22
BuildRequires:  go-md2man
BuildRequires:  podman
BuildRequires:  podman-docker
BuildRequires:  systemd-rpm-macros

Recommends:     %{name}-cli = %{version}-%{release}

%description
Cilium’s new Tetragon component enables powerful real-time, eBPF-based Security Observability and Runtime Enforcement.

Tetragon detects and is able to react to security-significant events, such as

- Process execution events
- System call activity
- I/O activity including network & file access

When used in a Kubernetes environment, Tetragon is Kubernetes-aware - that is, it understands Kubernetes identities such as namespaces, pods and so on - so that security event detection can be configured in relation to individual workloads.


%package cli
Summary:        Tetragon CLI tool
Provides:       %{cli_binname} = %{version}
# require same version for tetragon
Conflicts:      %{name} < %{version}-%{release}
Conflicts:      %{name} > %{version}-%{release}


%description cli
Cilium’s new Tetragon component enables powerful real-time, eBPF-based Security Observability and Runtime Enforcement.

Tetragon detects and is able to react to security-significant events, such as

- Process execution events
- System call activity
- I/O activity including network & file access

When used in a Kubernetes environment, Tetragon is Kubernetes-aware - that is, it understands Kubernetes identities such as namespaces, pods and so on - so that security event detection can be configured in relation to individual workloads.


%prep
%autosetup


%build
%make_build %{name} VERSION=%{version}
%make_build %{cli_binname} VERSION=%{version}

mkdir completion
./%{cli_binname} completion bash > completion/%{cli_binname}
./%{cli_binname} completion zsh > completion/_%{cli_binname}
./%{cli_binname} completion fish > completion/%{cli_binname}.fish


%install
install -Dpm 755 %{name} -t %{buildroot}%{_bindir}
install -Dpm 755 %{cli_binname} -t %{buildroot}%{_bindir}
install -Dpm 644 install/linux-tarball/systemd/%{name}.service -t %{buildroot}%{_unitdir}/
install -Dpm 644 install/linux-tarball/usr/local/lib/%{name}/%{name}.conf.d/* -t %{buildroot}%{_sysconfdir}/%{name}
install -Dpm 644 completion/%{cli_binname} -t %{buildroot}%{_datadir}/bash-completion/completions/
install -Dpm 644 completion/_%{cli_binname} -t %{buildroot}%{_datadir}/zsh/site-functions/
install -Dpm 644 completion/%{cli_binname}.fish -t %{buildroot}%{_datadir}/fish/vendor_completions.d/
install -DTpm 644 %{SOURCE101} %{buildroot}/%{_sysusersdir}/%{name}.conf


%pre
%sysusers_create_package %{name} %{SOURCE101}


%files
%license LICENSE
%doc *.md
%{_bindir}/%{name}
%config(noreplace) %{_sysconfdir}/%{name}
%{_sysusersdir}/%{name}.conf
%{_unitdir}/%{name}.service

%files cli
%license LICENSE
%doc *.md
%{_bindir}/%{cli_binname}
%{_datadir}/bash-completion/completions/%{cli_binname}
%{_datadir}/zsh/site-functions/_%{cli_binname}
%{_datadir}/fish/vendor_completions.d/%{cli_binname}.fish


%changelog
* Fri Dec 13 2024 Matyáš Kroupa <kroupa.matyas@gmail.com> - 1.3.0-1
- Update to 1.3.0

* Wed Nov 27 2024 Matyáš Kroupa <kroupa.matyas@gmail.com> - 1.2.1-1
- Update to 1.2.1

* Thu Sep 05 2024 Matyáš Kroupa <kroupa.matyas@gmail.com> - 1.2.0-1
- Update to 1.2.0

* Sun Jun 16 2024 Matyáš Kroupa <kroupa.matyas@gmail.com> - 1.1.2-2
- Fix executables permissions

* Sat Jun 15 2024 Matyáš Kroupa <kroupa.matyas@gmail.com>
- Initial version
