%global _missing_build_ids_terminate_build 0
%global debug_package %{nil}

Name:          operator-sdk
Version:       1.39.1
Release:       1%{?dist}
Summary:       SDK for building Kubernetes applications. Provides high level APIs, useful abstractions, and project scaffolding.

Group:         Development Tools
URL:           https://github.com/operator-framework/operator-sdk
License:       Apache-2.0

BuildRequires:  git
BuildRequires:  golang >= 1.21

Provides:       %{name} = %{version}
Provides:       helm-operator = %{version}

%description
This project is a component of the Operator Framework, an open source toolkit to manage Kubernetes native applications,
called Operators, in an effective, automated, and scalable way.

%prep
git clone -b v%{version} %{url}
%setup -n %{name} -T -D

%build
export GOPROXY="https://proxy.golang.org|direct"
export GOBIN="%{buildroot}/%{_bindir}"
%make_build build

mkdir completion
./build/%{name} completion bash > completion/%{name}
./build/%{name} completion zsh > completion/_%{name}
./build/%{name} completion fish > completion/%{name}.fish
./build/helm-operator completion bash > completion/helm-operator
./build/helm-operator completion zsh > completion/_helm-operator
./build/helm-operator completion fish > completion/helm-operator.fish

%install
export GOBIN="%{buildroot}/%{_bindir}"
%make_install
install -Dpm 644 completion/%{name} -t %{buildroot}%{_datadir}/bash-completion/completions/
install -Dpm 644 completion/_%{name} -t %{buildroot}%{_datadir}/zsh/site-functions/
install -Dpm 644 completion/%{name}.fish -t %{buildroot}%{_datadir}/fish/vendor_completions.d/
install -Dpm 644 completion/helm-operator -t %{buildroot}%{_datadir}/bash-completion/completions/
install -Dpm 644 completion/_helm-operator -t %{buildroot}%{_datadir}/zsh/site-functions/
install -Dpm 644 completion/helm-operator.fish -t %{buildroot}%{_datadir}/fish/vendor_completions.d/

%files
%license LICENSE
%doc README.md
%doc SECURITY.md
%{_bindir}/%{name}
%{_bindir}/helm-operator
%{_datadir}

%changelog
* Wed Jan 15 2025 Matyáš Kroupa <kroupa.matyas@gmail.com> - 1.39.1-1
- Update to 1.39.1

* Fri Jan 10 2025 Matyáš Kroupa <kroupa.matyas@gmail.com> - 1.39.0-1
- Update to 1.39.0

* Wed Nov 20 2024 Matyáš Kroupa <kroupa.matyas@gmail.com> - 1.38.0-1
- Update to 1.38.0

* Tue Sep 17 2024 Matyáš Kroupa <kroupa.matyas@gmail.com> - 1.37.0-1
- Update to 1.37.0

* Fri Aug 09 2024 Matyáš Kroupa <kroupa.matyas@gmail.com> - 1.36.1-1
- Update to 1.36.1

* Mon Jul 29 2024 Matyáš Kroupa <kroupa.matyas@gmail.com> - 1.36.0-1
- Update to 1.36.0

* Tue Jun 18 2024 Matyáš Kroupa <kroupa.matyas@gmail.com> - 1.35.0-1
- Update to 1.35.0

* Thu May 16 2024 Matyáš Kroupa <kroupa.matyas@gmail.com> - 1.34.2-1
- Update to 1.34.2

* Mon Mar 18 2024 Matyáš Kroupa <kroupa.matyas@gmail.com> - 1.34.1-2
- Add shell completions

* Sat Mar 09 2024 Matyáš Kroupa <kroupa.matyas@gmail.com> - 1.34.1-1
- Update to 1.34.1

* Sat Mar 02 2024 Matyáš Kroupa <kroupa.matyas@gmail.com> - 1.34.0-1
- Update to 1.34.0

* Sun Feb 18 2024 Matyáš Kroupa <kroupa.matyas@gmail.com> - 1.33.0-2
- Clone repo instead of downloading tarball

* Sun Feb 18 2024 Matyáš Kroupa <kroupa.matyas@gmail.com> - 1.33.0-1
- Bump to 1.33.0
- Build from source

* Wed Dec 08 2021 Matyáš Kroupa <kroupa.matyas@gmail.com> - 1.15.0-1
- Bump to 1.15.0

* Thu May 28 2020 Sergi Jimenez <tripledes@fedoraproject.org> - 0.17.1-1
- Bump to 0.17.1

* Wed Nov  6 2019 Sergi Jimenez <tripledes@fedoraproject.org> - 0.12.0-1
- Bump to 0.12.0

* Mon May 27 2019 Sergi Jimenez <tripledes@fedoraproject.org> - 0.8.1-2
- Fix dependencies

* Mon May 27 2019 Sergi Jimenez <tripledes@fedoraproject.org> - 0.8.1-1
- Initial import
