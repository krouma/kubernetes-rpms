%global debug_package %{nil}

%{?nodejs_find_provides_and_requires}

%global enable_tests 1

# don't require bundled modules
%global __requires_exclude_from ^(%{nodejs_sitelib}/%{name}/lib/.*|%{nodejs_sitelib}/%{name}/bin/yarn(|\\.cmd|\\.ps1|pkg.*))$

Name:           chectl
Version:        7.99.0
Release:        1%{?dist}
Summary:        CLI to manage Eclipse Che server and workspaces
License:        EPL-2.0
URL:            https://github.com/che-incubator/%{name}
#Source0:        %{url}/archive/refs/tags/%{version}.tar.gz

ExclusiveArch:  %{nodejs_arches}

BuildRequires:  git
BuildRequires:  nodejs-packaging
BuildRequires:  nodejs >= 18.0.0
BuildRequires:  npm
BuildRequires:  p7zip
BuildRequires:  p7zip-plugins
BuildRequires:  perl-Digest-SHA
BuildRequires:  yarnpkg

%description
CLI to manage Eclipse Che server and workspaces


%prep
git clone -b %{version} %{url}
%autosetup -p1 -T -D -n %{name}


%build
# use build script
yarn install
yarn pack-binaries


%install
install -d %{buildroot}%{nodejs_sitelib}/%{name}
cp -rp package.json lib bin node_modules templates \
   %{buildroot}%{nodejs_sitelib}/%{name}

mkdir -p %{buildroot}%{_bindir}
ln -sfr %{buildroot}%{nodejs_sitelib}/%{name}/bin/run %{buildroot}%{_bindir}/%{name}

# Fix the shebang in yarn.js because brp-mangle-shebangs fails to detect this properly (rhbz#1998924)
sed -e "s|^#!/usr/bin/env node$|#!/usr/bin/node|" \
    -i %{buildroot}%{nodejs_sitelib}/%{name}/bin/run

# Remove executable bits from bundled dependency tests
find %{buildroot}%{nodejs_sitelib}/%{name}/node_modules \
     -ipath '*/test/*' -type f -executable \
     -exec chmod -x '{}' +

%if 0%{?enable_tests}
%check
%nodejs_symlink_deps --check
%endif


%files
%doc README.md
%license LICENSE
%{_bindir}/%{name}
%{nodejs_sitelib}/%{name}/

%changelog
* Tue Feb 18 2025 Matyáš Kroupa <kroupa.matyas@gmail.com> - 7.99.0-1
- Update to 7.99.0

* Mon Jan 27 2025 Matyáš Kroupa <kroupa.matyas@gmail.com> - 7.98.0-1
- Update to 7.98.0

* Thu Jan 09 2025 Matyáš Kroupa <kroupa.matyas@gmail.com> - 7.97.0-1
- Update to 7.97.0

* Fri Dec 13 2024 Matyáš Kroupa <kroupa.matyas@gmail.com> - 7.96.0-1
- Update to 7.96.0

* Thu Nov 21 2024 Matyáš Kroupa <kroupa.matyas@gmail.com> - 7.95.0-1
- Update to 7.95.0

* Sat Nov 09 2024 Matyáš Kroupa <kroupa.matyas@gmail.com> - 7.94.0-1
- Update to 7.94.0

* Fri Oct 11 2024 Matyáš Kroupa <kroupa.matyas@gmail.com> - 7.93.0-1
- Update to 7.93.0

* Wed Sep 25 2024 Matyáš Kroupa <kroupa.matyas@gmail.com> - 7.92.0-1
- Update to 7.92.0

* Sun Sep 22 2024 Matyáš Kroupa <kroupa.matyas@gmail.com> - 7.91.0-3
- Explicitely require nodejs 18
- Rename nodejs-npm to npm

* Sat Sep 14 2024 Matyáš Kroupa <kroupa.matyas@gmail.com> - 7.91.0-2
- Add templates directory

* Sat Sep 14 2024 Matyáš Kroupa <kroupa.matyas@gmail.com>
- Initial version
