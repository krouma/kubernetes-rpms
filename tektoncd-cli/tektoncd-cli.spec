%global _missing_build_ids_terminate_build 0
%global debug_package %{nil}
%define binname tkn

Name:           tektoncd-cli
Version:        0.40.0
Release:        1%{?dist}
Summary:        A CLI for interacting with Tekton!

License:        Apache-2.0
URL:            https://github.com/tektoncd/cli
Source0:        %{url}/archive/refs/tags/v%{version}.tar.gz

BuildRequires:  git
BuildRequires:  golang >= 1.22
BuildRequires:  go-md2man

Provides:       %{name} = %{version}

%description
The Tekton Pipelines CLI project provides a command-line interface (CLI) for interacting with Tekton, an open-source framework for Continuous Integration and Delivery (CI/CD) systems.


%prep
%autosetup -n cli-%{version}


%build
%make_build bin/%{binname}

mkdir completion
./bin/%{binname} completion bash > completion/%{binname}
./bin/%{binname} completion zsh > completion/_%{binname}
./bin/%{binname} completion fish > completion/%{binname}.fish


%install
rm -rf $RPM_BUILD_ROOT
install -Dpm 755 bin/%{binname} -t %{buildroot}%{_bindir}/
install -Dpm 644 completion/%{binname} -t %{buildroot}%{_datadir}/bash-completion/completions/
install -Dpm 644 completion/_%{binname} -t %{buildroot}%{_datadir}/zsh/site-functions/
install -Dpm 644 completion/%{binname}.fish -t %{buildroot}%{_datadir}/fish/vendor_completions.d/


%files
%license LICENSE
%doc *.md
%{_bindir}/%{binname}
%{_datadir}


%changelog
* Wed Feb 26 2025 Matyáš Kroupa <kroupa.matyas@gmail.com> - 0.40.0-1
- Update to 0.40.0

* Thu Jan 30 2025 Matyáš Kroupa <kroupa.matyas@gmail.com> - 0.39.1-1
- Update to 0.39.1

* Tue Nov 26 2024 Matyáš Kroupa <kroupa.matyas@gmail.com> - 0.39.0-1
- Update to 0.39.0

* Sat Sep 14 2024 Matyáš Kroupa <kroupa.matyas@gmail.com>
- Initial version
