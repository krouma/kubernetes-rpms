%global _missing_build_ids_terminate_build 0
%global debug_package %{nil}

%global commit_sha 1aa46c5

Name:          crc
Version:       2.48.0
Release:       1%{?dist}
Summary:       A tool for running local OpenShift cluster

Group:         Development Tools
License:       Apache-2.0
URL:           https://github.com/crc-org/crc
Source0:        %{url}/archive/refs/tags/v%{version}.tar.gz

BuildRequires:  golang >= 1.20
BuildRequires:  gpgme-devel

Provides:       %{name} = %{version}

%description
CRC is a tool to help you run containers. It manages a local OpenShift 4.x cluster, Microshift or a Podman VM optimized for testing and development purposes

%prep
%autosetup

%build
export GOPROXY="https://proxy.golang.org|direct"
export GOBIN=$(pwd)
%make_build COMMIT_SHA=%{commit_sha} SOURCES=""

mkdir completion
./%{name} completion bash > completion/%{name}
./%{name} completion zsh > completion/_%{name}
./%{name} completion fish > completion/%{name}.fish

%install
install -Dpm 755 %{name} -t %{buildroot}%{_bindir}
install -Dpm 644 completion/%{name} -t %{buildroot}%{_datadir}/bash-completion/completions/
install -Dpm 644 completion/_%{name} -t %{buildroot}%{_datadir}/zsh/site-functions/
install -Dpm 644 completion/%{name}.fish -t %{buildroot}%{_datadir}/fish/vendor_completions.d/

%files
%license LICENSE
%doc README.md
%{_bindir}/%{name}
%{_datadir}

%changelog
* Fri Mar 07 2025 Matyáš Kroupa <kroupa.matyas@gmail.com> - 2.48.0-1
- Update to 2.48.0

* Mon Feb 17 2025 Matyáš Kroupa <kroupa.matyas@gmail.com> - 2.47.0-2
- Fix readme file extension

* Fri Feb 14 2025 Matyáš Kroupa <kroupa.matyas@gmail.com> - 2.47.0-1
- Update to 2.47.0

* Tue Jan 21 2025 Matyáš Kroupa <kroupa.matyas@gmail.com> - 2.46.0-1
- Update to 2.46.0

* Wed Dec 18 2024 Matyáš Kroupa <kroupa.matyas@gmail.com> - 2.45.0-1
- Update to 2.45.0

* Fri Nov 22 2024 Matyáš Kroupa <kroupa.matyas@gmail.com> - 2.44.0-1
- Update to 2.44.0

* Wed Oct 30 2024 Matyáš Kroupa <kroupa.matyas@gmail.com> - 2.43.0-1
- Update to 2.43.0

* Thu Oct 10 2024 Matyáš Kroupa <kroupa.matyas@gmail.com> - 2.42.0-1
- Update to 2.42.0

* Fri Aug 30 2024 Matyáš Kroupa <kroupa.matyas@gmail.com> - 2.41.0-1
- Update to 2.41.0

* Wed Aug 07 2024 Matyáš Kroupa <kroupa.matyas@gmail.com> - 2.40.0-1
- Update to 2.40.0

* Thu Jul 18 2024 Matyáš Kroupa <kroupa.matyas@gmail.com> - 2.39.0-1
- Update to 2.39.0

* Thu Jun 27 2024 Matyáš Kroupa <kroupa.matyas@gmail.com> - 2.38.0-1
- Update to 2.38.0

* Thu Jun 06 2024 Matyáš Kroupa <kroupa.matyas@gmail.com> - 2.37.1-1
- Update to 2.37.1

* Wed May 15 2024 Matyáš Kroupa <kroupa.matyas@gmail.com> - 2.36.0-1
- Update to 2.36.0

* Thu May 02 2024 Matyáš Kroupa <kroupa.matyas@gmail.com> - 2.35.0-1
- Update to 2.35.0

* Wed Apr 03 2024 Matyáš Kroupa <kroupa.matyas@gmail.com> - 2.34.1-1
- Update to 2.34.1

* Mon Apr 01 2024 Matyáš Kroupa <kroupa.matyas@gmail.com> - 2.33.0-2
- Fix binary permissions

* Sun Mar 31 2024 Matyáš Kroupa <kroupa.matyas@gmail.com> - 2.33.0-1
- Initial version
