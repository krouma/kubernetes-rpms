%global debug_package %{nil}

Name:           odo
Version:        3.16.1
Release:        1%{?dist}
Summary:        Developer-focused CLI for fast & iterative container-based application development on Podman and Kubernetes. Implementation of the open Devfile standard.

License:        Apache-2.0
URL:            https://github.com/redhat-developer/%{name}

BuildRequires:  git
BuildRequires:  golang >= 1.19

Provides:       %{name} = %{version}

%description
odo is a fast, and iterative CLI tool for container-based application development. It is an implementation of the open Devfile standard, supporting Podman, Kubernetes and OpenShift.


%prep
git clone -b v%{version} %{url}
%setup -n %{name} -T -D


%build
%make_build

mkdir completion
./%{name} completion bash > completion/%{name}
./%{name} completion zsh > completion/_%{name}
./%{name} completion fish > completion/%{name}.fish


%install
export GOBIN="%{buildroot}/%{_bindir}"
%make_install
install -Dpm 644 completion/%{name} -t %{buildroot}%{_datadir}/bash-completion/completions/
install -Dpm 644 completion/_%{name} -t %{buildroot}%{_datadir}/zsh/site-functions/
install -Dpm 644 completion/%{name}.fish -t %{buildroot}%{_datadir}/fish/vendor_completions.d/


%files
%license LICENSE
%doc *.md
%{_bindir}/%{name}
%{_datadir}


%changelog
* Wed Jun 26 2024 Matyáš Kroupa <kroupa.matyas@gmail.com> - 3.16.1-1
- Update to 3.16.1

* Mon Jun 17 2024 Matyáš Kroupa <kroupa.matyas@gmail.com> - 3.16.0-1
- Update to 3.16.0

* Sun Mar 31 2024 Matyáš Kroupa <kroupa.matyas@gmail.com> - 3.15.0-2
- Add shell completions

* Mon Feb 19 2024 Matyáš Kroupa <kroupa.matyas@gmail.com>
- Initial version
