%global _missing_build_ids_terminate_build 0
%global debug_package %{nil}

Name:           kubelogin
Version:        0.1.7
Release:        1%{?dist}
Summary:        A Kubernetes credential (exec) plugin implementing azure authentication

License:        Apache-2.0
URL:            https://github.com/Azure/%{name}
Source0:        %{url}/archive/refs/tags/v%{version}.tar.gz

BuildRequires:  git
BuildRequires:  golang >= 1.21

Provides:       %{name} = %{version}

%description
kubelogin is a client-go credential (exec) plugin implementing azure authentication. This plugin provides features that are not available in kubectl. It is supported on kubectl v1.11+


%prep
%autosetup


%build
%make_build %{name} GIT_TAG=%{version} OS=%{_os} ARCH=%{_arch} GOARM=""

mkdir completion
bin/linux_%{_arch}/%{name} completion bash > completion/%{name}
bin/linux_%{_arch}/%{name} completion zsh > completion/_%{name}
bin/linux_%{_arch}/%{name} completion fish > completion/%{name}.fish


%install
rm -rf $RPM_BUILD_ROOT
install -Dpm 755 bin/linux_%{_arch}/%{name} -t %{buildroot}%{_bindir}
install -Dpm 644 completion/%{name} -t %{buildroot}%{_datadir}/bash-completion/completions/
install -Dpm 644 completion/_%{name} -t %{buildroot}%{_datadir}/zsh/site-functions/
install -Dpm 644 completion/%{name}.fish -t %{buildroot}%{_datadir}/fish/vendor_completions.d/


%files
%license LICENSE
%doc *.md
%{_bindir}/%{name}
%{_datadir}


%changelog
* Thu Jan 30 2025 Matyáš Kroupa <kroupa.matyas@gmail.com> - 0.1.7-1
- Update to 0.1.7

* Sat Dec 14 2024 Matyáš Kroupa <kroupa.matyas@gmail.com> - 0.1.6-1
- Update to 0.1.6

* Tue Dec 03 2024 Matyáš Kroupa <kroupa.matyas@gmail.com> - 0.1.5-1
- Update to 0.1.5

* Sun Sep 01 2024 Matyáš Kroupa <kroupa.matyas@gmail.com>
- Initial version
