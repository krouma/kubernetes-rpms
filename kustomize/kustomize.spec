%global debug_package %{nil}

Name:           kustomize
Version:        5.6.0
Release:        1%{?dist}
Summary:        Customization of kubernetes YAML configurations
License:        Apache-2.0
URL:            https://github.com/kubernetes-sigs/%{name}

Source0:        %{url}/archive/refs/tags/%{name}/v%{version}.tar.gz

BuildRequires:  git
BuildRequires:  golang
BuildRequires:  make

%description
Kustomize lets you customize raw, template-free YAML files for multiple purposes, leaving the original YAML untouched and usable as is.

Kustomize targets kubernetes; it understands and can patch kubernetes style API objects. It's like make, in that what it does is declared in a file, and it's like sed, in that it emits edited text.


%prep
%autosetup -n %{name}-%{name}-v%{version}


%build
# Fake repo for the build process
git config --global user.email "you@example.com"
git config --global user.name "Your Name"
git init
git add --all
git commit -q -m "Fake commit for version %{version}"
git tag v%{version}

export GOBIN=$(pwd)/%{name}
export KUSTOMIZE_ROOT=$(pwd)
%make_build %{name}

mkdir completion
./%{name}/%{name} completion bash > completion/%{name}
./%{name}/%{name} completion zsh > completion/_%{name}
./%{name}/%{name} completion fish > completion/%{name}.fish


%install
install -D -p -m 0755 %{name}/%{name} %{buildroot}%{_bindir}/%{name}
install -Dpm 644 completion/%{name} -t %{buildroot}%{_datadir}/bash-completion/completions/
install -Dpm 644 completion/_%{name} -t %{buildroot}%{_datadir}/zsh/site-functions/
install -Dpm 644 completion/%{name}.fish -t %{buildroot}%{_datadir}/fish/vendor_completions.d/


%files
%license LICENSE
%doc README.md
%{_bindir}/%{name}
%{_datadir}

%changelog
* Tue Jan 14 2025 Matyáš Kroupa <kroupa.matyas@gmail.com> - 5.6.0-1
- Update to 5.6.0

* Wed Oct 09 2024 Matyáš Kroupa <kroupa.matyas@gmail.com> - 5.5.0-1
- Update to 5.5.0

* Fri Jul 19 2024 Matyáš Kroupa <kroupa.matyas@gmail.com> - 5.4.3-1
- Update to 5.4.3

* Wed May 22 2024 Matyáš Kroupa <kroupa.matyas@gmail.com> - 5.4.2-1
- Update to 5.4.2

* Wed May 01 2024 Matyáš Kroupa <kroupa.matyas@gmail.com> - 5.4.1-1
- Initial version
