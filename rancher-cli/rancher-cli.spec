%global debug_package %{nil}

Name:           rancher-cli
Version:        2.10.1
Release:        1%{?dist}
Summary:        Unified tool for interacting with your Rancher Server
License:        ASL 2.0
URL:            https://rancher.com/docs/rancher/v2.x/en/cli/

Source0:        https://github.com/rancher/cli/archive/v%{version}.tar.gz#/%{name}-%{version}.tar.gz

BuildRequires:  git
BuildRequires:  golang >= 1.22

%description
The Rancher Command Line Interface (CLI) is a unified tool for interacting with
your Rancher Server.

%prep
%autosetup -n cli-%{version}

%build
go build

%install
install -D -p -m 755 cli %{buildroot}%{_bindir}/rancher

%files
%license LICENSE
%doc README.md
%{_bindir}/rancher

%changelog
* Fri Dec 20 2024 Matyáš Kroupa <kroupa.matyas@gmail.com> - 2.10.1-1
- Update to 2.10.1

* Tue Nov 19 2024 Matyáš Kroupa <kroupa.matyas@gmail.com> - 2.10.0-1
- Update to 2.10.0

* Fri Sep 20 2024 Matyáš Kroupa <kroupa.matyas@gmail.com> - 2.9.2-1
- Update to 2.9.2

* Tue Jul 23 2024 Matyáš Kroupa <kroupa.matyas@gmail.com> - 2.9.0-1
- Update to 2.9.0

* Thu May 16 2024 Matyáš Kroupa <kroupa.matyas@gmail.com> - 2.8.4-1
- Update to 2.8.4

* Mon Mar 18 2024 Matyáš Kroupa <kroupa.matyas@gmail.com> - 2.8.3-1
- Update to 2.8.3

* Mon Feb 12 2024 Matyáš Kroupa <kroupa.matyas@gmail.com> - 2.8.0-1
- Update to 2.8.0

* Fri Nov 11 2022 Matyáš Kroupa <kroupa.matyas@gmail.com> - 2.7.0-1
- Update to 2.7.0

* Fri Aug 19 2022 Matyáš Kroupa <kroupa.matyas@gmail.com> - 2.6.7-1
- Update to 2.6.7

* Wed May 11 2022 Matyáš Kroupa <kroupa.matyas@gmail.com> - 2.6.5-1
- Update to 2.6.5

* Thu Jan 20 2022 Matyáš Kroupa <kroupa.matyas@gmail.com> - 2.6.0-1
- Update to 2.6.0

* Fri Dec 10 2021 Matyáš Kroupa <kroupa.matyas@gmail.com> - 2.4.13-1
- Update to 2.4.13

* Sun Jul 25 2021 Matyáš Kroupa <kroupa.matyas@gmail.com> - 2.4.10-1
- Update to 2.4.10

* Fri Dec 04 2020 Simone Caronni <negativo17@gmail.com> - 2.4.9-1
- Update to 2.4.9.

* Wed Oct 07 2020 Simone Caronni <negativo17@gmail.com> - 2.4.6-1
- Update to 2.4.6.

* Tue Jun 23 2020 Simone Caronni <negativo17@gmail.com> - 2.4.5-1
- Update to 2.4.5.

* Fri May 29 2020 Simone Caronni <negativo17@gmail.com> - 2.4.3-1
- First build.
