%global debug_package %{nil}

Name:           helm
Version:        3.17.1
Release:        1%{?dist}
Summary:        The Kubernetes Package Manager.
License:        ASL 2.0
URL:            https://helm.sh

Source0:        https://github.com/%{name}/%{name}/archive/refs/tags/v%{version}.tar.gz

BuildRequires:  git
BuildRequires:  golang
BuildRequires:  make

%description
Helm is a tool for managing Charts. Charts are packages of pre-configured
Kubernetes resources.

Use Helm to:

- Find and use popular software packaged as Helm Charts to run in Kubernetes
- Share your own applications as Helm Charts
- Create reproducible builds of your Kubernetes applications
- Intelligently manage your Kubernetes manifest files
- Manage releases of Helm packages

%prep
%autosetup

%build
%make_build

%install
install -D -p -m 0755 bin/%{name} %{buildroot}%{_bindir}/%{name}

%files
%license LICENSE
%doc README.md
%{_bindir}/%{name}

%changelog
* Thu Feb 13 2025 Matyáš Kroupa <kroupa.matyas@gmail.com> - 3.17.1-1
- Update to 3.17.1

* Thu Jan 16 2025 Matyáš Kroupa <kroupa.matyas@gmail.com> - 3.17.0-1
- Update to 3.17.0

* Mon Dec 16 2024 Matyáš Kroupa <kroupa.matyas@gmail.com> - 3.16.4-1
- Update to 3.16.4

* Thu Oct 10 2024 Matyáš Kroupa <kroupa.matyas@gmail.com> - 3.16.2-1
- Update to 3.16.2

* Fri Sep 13 2024 Matyáš Kroupa <kroupa.matyas@gmail.com> - 3.16.1-1
- Update to 3.16.1

* Wed Sep 11 2024 Matyáš Kroupa <kroupa.matyas@gmail.com> - 3.16.0-1
- Update to 3.16.0

* Wed Aug 14 2024 Matyáš Kroupa <kroupa.matyas@gmail.com> - 3.15.4-1
- Update to 3.15.4

* Sun Jul 14 2024 Matyáš Kroupa <kroupa.matyas@gmail.com> - 3.15.3-1
- Update to 3.15.3

* Wed Jun 12 2024 Matyáš Kroupa <kroupa.matyas@gmail.com> - 3.15.2-1
- Update to 3.15.2

* Thu May 23 2024 Matyáš Kroupa <kroupa.matyas@gmail.com> - 3.15.1-1
- Update to 3.15.1

* Wed May 15 2024 Matyáš Kroupa <kroupa.matyas@gmail.com> - 3.15.0-1
- Update to 3.15.0

* Thu Apr 11 2024 Matyáš Kroupa <kroupa.matyas@gmail.com> - 3.14.4-1
- Update to 3.14.4

* Wed Mar 13 2024 Matyáš Kroupa <kroupa.matyas@gmail.com> - 3.14.3-1
- Update to 3.14.3

* Wed Feb 21 2024 Matyáš Kroupa <kroupa.matyas@gmail.com> - 3.14.2-1
- Update to 3.14.2

* Sun Feb 18 2024 Matyáš Kroupa <kroupa.matyas@gmail.com> - 3.14.1-1
- Update to 3.14.1

* Thu Jan 18 2024 Matyáš Kroupa <kroupa.matyas@gmail.com> - 3.14.0-1
- Update to 3.14.0

* Wed Dec 13 2023 Matyáš Kroupa <kroupa.matyas@gmail.com> - 3.13.3-1
- Update to 3.13.3

* Wed Nov 08 2023 Matyáš Kroupa <kroupa.matyas@gmail.com> - 3.13.2-1
- Update to 3.13.2

* Thu Oct 12 2023 Matyáš Kroupa <kroupa.matyas@gmail.com> - 3.13.1-1
- Update to 3.13.1

* Thu Sep 28 2023 Matyáš Kroupa <kroupa.matyas@gmail.com> - 3.13.0-1
- Update to 3.13.0

* Fri Aug 11 2023 Matyáš Kroupa <kroupa.matyas@gmail.com> - 3.12.3-1
- Update to 3.12.3

* Tue Jul 25 2023 Matyáš Kroupa <kroupa.matyas@gmail.com> - 3.12.2-1
- Update to 3.12.2

* Wed Jun 21 2023 Matyáš Kroupa <kroupa.matyas@gmail.com> - 3.12.1-1
- Update to 3.12.1

* Sun Feb 12 2023 Matyáš Kroupa <kroupa.matyas@gmail.com> - 3.11.1-1
- Update to 3.11.1

* Wed Jan 25 2023 Matyáš Kroupa <kroupa.matyas@gmail.com> - 3.11.0-1
- Update to 3.11.0

* Tue Dec 20 2022 Matyáš Kroupa <kroupa.matyas@gmail.com> - 3.10.3-1
- Update to 3.10.3

* Fri Dec 02 2022 Matyáš Kroupa <kroupa.matyas@gmail.com> - 3.10.2-2
- Fix Source0 url

* Fri Nov 11 2022 Matyáš Kroupa <kroupa.matyas@gmail.com> - 3.10.2-1
- Update to 3.10.2

* Thu Oct 13 2022 Matyáš Kroupa <kroupa.matyas@gmail.com> - 3.10.1-1
- Update to 3.10.1

* Thu Sep 22 2022 Matyáš Kroupa <kroupa.matyas@gmail.com> - 3.10.0-1
- Update to 3.10.0

* Thu Jul 21 2022 Matyáš Kroupa <kroupa.matyas@gmail.com> - 3.9.2-1
- Update to 3.9.2

* Mon Jul 18 2022 Matyáš Kroupa <kroupa.matyas@gmail.com> - 3.9.1-1
- Update to 3.9.1

* Thu May 19 2022 Matyáš Kroupa <kroupa.matyas@gmail.com> - 3.9.0-1
- Update to 3.9.0

* Fri Apr 15 2022 Matyáš Kroupa <kroupa.matyas@gmail.com> - 3.8.2-1
- Update to 3.8.2

* Fri Mar 11 2022 Matyáš Kroupa <kroupa.matyas@gmail.com> - 3.8.1-1
- Update to 3.8.1

* Tue Jan 25 2022 Matyáš Kroupa <kroupa.matyas@gmail.com> - 3.8.0-1
- Update to 3.8.0

* Fri Dec 10 2021 Matyáš Kroupa <kroupa.matyas@gmail.com> - 3.7.2-1
- Update to 3.7.2

* Fri Oct 29 2021 Matyáš Kroupa <kroupa.matyas@gmail.com> - 3.7.1-1
- Update to 3.7.1.

* Fri Jul 23 2021 Matyáš Kroupa <kroupa.matyas@gmail.com> - 3.6.3-1
- Update to 3.6.3.

* Mon Jun 21 2021 Matyáš Kroupa <kroupa.matyas@gmail.com> - 3.6.1-1
- Update to 3.6.1.

* Thu May 27 2021 Matyáš Kroupa <kroupa.matyas@gmail.com> - 3.6.0-1
- Update to 3.6.0.

* Thu Apr 22 2021 Matyáš Kroupa <kroupa.matyas@gmail.com> - 3.5.4-1
- Update to 3.5.4.

* Sat Mar 13 2021 Matyáš Kroupa <kroupa.matyas@gmail.com> - 3.5.3-1
- Update to 3.5.3.

* Sat Mar 06 2021 Matyáš Kroupa <kroupa.matyas@gmail.com> - 3.5.2-1
- Update to 3.5.2.

* Mon Jan 18 2021 Simone Caronni <negativo17@gmail.com> - 3.5.0-1
- Update to 3.5.0.

* Fri Dec 04 2020 Simone Caronni <negativo17@gmail.com> - 3.4.1-1
- Update to 3.4.1.

* Wed Oct 07 2020 Simone Caronni <negativo17@gmail.com> - 3.3.4-1
- Update to 3.3.4.

* Mon Aug 17 2020 Simone Caronni <negativo17@gmail.com> - 3.3.0-1
- Update to 3.3.0.

* Wed Jun 17 2020 Simone Caronni <negativo17@gmail.com> - 3.2.4-1
- Update to 3.2.4.

* Mon Jun 08 2020 Simone Caronni <negativo17@gmail.com> - 3.2.2-1
- Update to 3.2.2.

* Thu May 28 2020 Simone Caronni <negativo17@gmail.com> - 3.2.1-1
- First build.
